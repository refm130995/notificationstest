import * as firebase from "firebase/app";
import "firebase/messaging";
const initializedFirebaseApp = firebase.initializeApp({
	// Project Settings => Add Firebase to your web app
    messagingSenderId: "134068650153"
});
const messaging = initializedFirebaseApp.messaging();
messaging.usePublicVapidKey(
	// Project Settings => Cloud Messaging => Web Push certificates
  "BM1hlCP_LEN-2izdvWMiQ7dtkKU8v7uJNsDwjm-Qf6EZsNiPEGWLChxAGQ33BiKAUBHVD_9IslEmeNrNLpJK8BU"
);
export default messaging;